# This program says hello, and asks for my name.

print("Hello, World!")

myName = input("What is your name?\n> ")

print("It's good to meet you, {}".format(myName))
print("The length of your name is: {} characters".format(len(myName)))

myAge = input("How old are you?\n> ")

print("You will be {} next year. Better buy more candles.".format(int(myAge) + 1))
